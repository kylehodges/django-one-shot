from django.shortcuts import render, redirect, get_object_or_404, get_list_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm
# Create your views here.


def todo_list_list(request):
    todo_list = TodoList.objects.all()

    context = {
        'todo_list': todo_list
    }
    return render(request, 'todos/list.html', context)


def todo_list_detail(request, id):
    td_list = get_object_or_404(TodoList, id=id)

    context = {
        'td_list': td_list,
    }
    return render(request, 'todos/detail.html', context)

def todo_list_create(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            td_list = form.save()
            return redirect('todo_list_detail', id=td_list.id)
    else:
        form = TodoListForm()

        context = {
            'form': form
    }
    return render(request, 'todos/create.html', context)

def todo_list_update(request, id):
    td_list = TodoList.objects.get(id=id)
    if request.method == 'POST':
        form = TodoListForm(request.POST, instance=td_list)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=id)
    else:
        form = TodoListForm(instance=td_list)

    context = {
        'form': form,
        'td_list': td_list
    }
    return render(request, 'todos/update.html', context)

def todo_list_delete(request, id):
    td_list = TodoList.objects.get(id=id)
    if request.method == 'POST':
        td_list.delete()
        return redirect('todo_list_list')

    return render(request, 'todos/delete.html')

def todo_item_create(request):
    if request.method == 'POST':
        form = TodoItemForm(request.POST)
        if form.is_valid():
            td_list = form.save()
        return redirect('todo_list_detail', id=td_list.list.id)

    else:
        form = TodoItemForm()

    context = {
        'form': form,
    }
    return render(request, 'todos/itemscreate.html', context)

def todo_item_update(request, id):
    item = TodoItem.objects.get(id=id)

    if request.method == 'POST':
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=item.list.id)
    else:
        form = TodoItemForm(instance=item)

    context = {
        'form': form,
        'item': item
    }
    return render(request, 'todos/itemupdate.html', context)
